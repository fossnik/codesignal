import java.util.HashSet;

class Example {
	boolean sudoku(int[][] grid) {

		// test each 3x3 square includes 0 - 9
		for (int y = 0; y < grid.length; y += 3) {
			for (int x = 0; x < grid[0].length; x += 3) {
				HashSet<Integer> unique = new HashSet<>();
				for (int i = y; i < y + 3; i++) {
					for (int j = x; j < x + 3; j++) {
						unique.add(grid[i][j]);
					}
				}
				if (unique.size() != 9) return false;
			}
		}

		// test each row includes 0 - 9
		for (int y = 0; y < grid.length; y++) {
			HashSet<Integer> unique = new HashSet<>();
			for (int x = 0; x < grid[0].length; x++) {
				unique.add(grid[y][x]);
			}
			if (unique.size() != 9) return false;
		}

		// test each column includes 0 - 9
		for (int x = 0; x < grid[0].length; x++) {
			HashSet<Integer> unique = new HashSet<>();
			for (int y = 0; y < grid.length; y++) {
				unique.add(grid[y][x]);
			}
			if (unique.size() != 9) return false;
		}

		return true;
	}

}
