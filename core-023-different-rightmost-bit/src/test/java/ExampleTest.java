import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(2, example.differentRightmostBit(11, 13));
	}

	@Test
	public void test2() {
		assertEquals(16, example.differentRightmostBit(7, 23));
	}

	@Test
	public void test3() {
		assertEquals(1, example.differentRightmostBit(1, 0));
	}

	@Test
	public void test4() {
		assertEquals(1, example.differentRightmostBit(64, 65));
	}

	@Test
	public void test5() {
		assertEquals(131072, example.differentRightmostBit(1073741823, 1071513599));
	}

	@Test
	public void test6() {
		assertEquals(4, example.differentRightmostBit(42, 22));
	}
}
