import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(2, example.equalPairOfBits(10, 11));
	}

	@Test
	public void test2() {
		assertEquals(1, example.equalPairOfBits(0, 0));
	}

	@Test
	public void test3() {
		assertEquals(8, example.equalPairOfBits(28, 27));
	}

	@Test
	public void test4() {
		assertEquals(32, example.equalPairOfBits(895, 928));
	}

	@Test
	public void test5() {
		assertEquals(262144, example.equalPairOfBits(1073741824, 1006895103));
	}
}
