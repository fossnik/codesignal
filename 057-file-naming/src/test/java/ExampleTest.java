import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		String[] input = new String[]{
				"doc",
				"doc",
				"image",
				"doc(1)",
				"doc"
		};

		String[] expected = new String[]{
				"doc",
				"doc(1)",
				"image",
				"doc(1)(1)",
				"doc(2)"
		};
		assertArrayEquals(expected, example.fileNaming(input));
	}

	@Test
	public void test2() {
		String[] input = new String[]{
				"a(1)",
				"a(6)",
				"a",
				"a",
				"a",
				"a",
				"a",
				"a",
				"a",
				"a",
				"a",
				"a"
		};

		String[] expected = new String[]{
				"a(1)",
				"a(6)",
				"a",
				"a(2)",
				"a(3)",
				"a(4)",
				"a(5)",
				"a(7)",
				"a(8)",
				"a(9)",
				"a(10)",
				"a(11)"
		};
		assertArrayEquals(expected, example.fileNaming(input));
	}

	@Test
	public void test3() {
		String[] input = new String[]{
				"dd",
				"dd(1)",
				"dd(2)",
				"dd",
				"dd(1)",
				"dd(1)(2)",
				"dd(1)(1)",
				"dd",
				"dd(1)"
		};

		String[] expected = new String[]{
				"dd",
				"dd(1)",
				"dd(2)",
				"dd(3)",
				"dd(1)(1)",
				"dd(1)(2)",
				"dd(1)(1)(1)",
				"dd(4)",
				"dd(1)(3)"
		};
		assertArrayEquals(expected, example.fileNaming(input));
	}

	@Test
	public void test4() {
		String[] input = new String[]{
				"a",
				"b",
				"cd",
				"b ",
				"a(3)"
		};

		String[] expected = new String[]{
				"a",
				"b",
				"cd",
				"b ",
				"a(3)"
		};
		assertArrayEquals(expected, example.fileNaming(input));
	}

	@Test
	public void test5() {
		String[] input = new String[]{
				"name",
				"name",
				"name(1)",
				"name(3)",
				"name(2)",
				"name(2)"
		};

		String[] expected = new String[]{
				"name",
				"name(1)",
				"name(1)(1)",
				"name(3)",
				"name(2)",
				"name(2)(1)"
		};
		assertArrayEquals(expected, example.fileNaming(input));
	}

	@Test
	public void test6() {
		String[] input = new String[]{};

		String[] expected = new String[]{};

		assertArrayEquals(expected, example.fileNaming(input));
	}

}
