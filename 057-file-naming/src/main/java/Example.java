import java.util.LinkedHashSet;

class Example {
	String[] fileNaming(String[] names) {
		LinkedHashSet<String> fileNames = new LinkedHashSet<>();

		for (String fileName : names) {
			if (fileNames.contains(fileName)) {
				int count = 1;
				while (fileNames.contains(String.format("%s(%s)", fileName, count))) {
					count++;
				}
				fileNames.add(String.format("%s(%s)", fileName, count));
			} else {
				fileNames.add(fileName);
			}
		}

		return fileNames.toArray(new String[fileNames.size()]);
	}
}
