import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(21784, example.arrayPacking(new int[]{24, 85, 0}));
	}

	@Test
	public void test2() {
		assertEquals(2567447, example.arrayPacking(new int[]{23, 45, 39}));
	}

	@Test
	public void test3() {
		assertEquals(134480385, example.arrayPacking(new int[]{1, 2, 4, 8}));
	}

	@Test
	public void test4() {
		assertEquals(5, example.arrayPacking(new int[]{5}));
	}

	@Test
	public void test5() {
		assertEquals(724198331, example.arrayPacking(new int[]{187, 99, 42, 43}));
	}
}
