import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

class Example {
	int arrayPacking(int[] a) {
		List<String> numbers = Arrays.stream(a).mapToObj(Integer::toString).collect(Collectors.toList());

		Collections.reverse(numbers);

		String[] outputArray = numbers.stream().mapToInt(Integer::parseInt).mapToObj(i -> Integer.toString(i, 2)).toArray(String[]::new);

		String output = "";
		for(String string : outputArray) {
			output += String.format("%8s", string).replace(' ', '0');
		}

		return Integer.parseInt(output, 2);
	}
}
