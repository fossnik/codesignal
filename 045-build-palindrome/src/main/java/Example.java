class Example {
	String buildPalindrome(String st) {
		for (int i = st.length() / 2;; i++) {
			String firstHalf = splitInput(st, i)[0];
			String secondHalf = new StringBuilder(firstHalf).reverse().toString();

			String trial = firstHalf + secondHalf;
			String trial2 = firstHalf + secondHalf.substring(1);
			if (isPalindrome(trial2) && trial2.contains(st)) return trial2;
			if (isPalindrome(trial) && trial.contains(st)) return trial;
		}
	}

	private String[] splitInput(String inputString, int segmentSize) {
		String re = "(?<=\\G.{" + segmentSize + "})";
		return inputString.split(re);
	}

	private boolean isPalindrome(String a) {
		return a.equals(new StringBuilder(a).reverse().toString());
	}
}
