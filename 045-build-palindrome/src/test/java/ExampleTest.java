import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals("abcdcba", example.buildPalindrome("abcdc"));
	}

	@Test
	public void test2() {
		assertEquals("abababa", example.buildPalindrome("ababab"));
	}

	@Test
	public void test3() {
		assertEquals("abba", example.buildPalindrome("abba"));
	}

	@Test
	public void test4() {
		assertEquals("abaaba", example.buildPalindrome("abaa"));
	}

	@Test
	public void test5() {
		assertEquals("aaaabaaaa", example.buildPalindrome("aaaaba"));
	}

	@Test
	public void test6() {
		assertEquals("abcba", example.buildPalindrome("abc"));
	}

	@Test
	public void test7() {
		assertEquals("kebabek", example.buildPalindrome("kebab"));
	}

	@Test
	public void test8() {
		assertEquals("abccba", example.buildPalindrome("abccba"));
	}

	@Test
	public void test9() {
		assertEquals("abcabcbacba", example.buildPalindrome("abcabc"));
	}

	@Test
	public void test10() {
		assertEquals("cbdbedffcgcffdebdbc", example.buildPalindrome("cbdbedffcg"));
	}

	@Test
	public void test11() {
		assertEquals("euotmnmtoue", example.buildPalindrome("euotmn"));
	}
}
