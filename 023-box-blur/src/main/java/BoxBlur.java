class BoxBlur {
	int[][] boxBlur(int[][] image) {
		int[][] newImage = new int[image.length - 2][image[0].length - 2];

		for (int y = 0; y < newImage.length; y++) {
			for (int x = 0; x < newImage[0].length; x++) {
				newImage[y][x] = (
						image[y][x] +
								image[y][x + 1] +
								image[y][x + 2] +
								image[y + 1][x] +
								image[y + 1][x + 1] +
								image[y + 1][x + 2] +
								image[y + 2][x] +
								image[y + 2][x + 1] +
								image[y + 2][x + 2]
				) / 9;
			}
		}

		return newImage;
	}

}