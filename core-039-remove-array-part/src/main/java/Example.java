class Example {
	int[] removeArrayPart(int[] inputArray, int l, int r) {
		int[] newArray = new int[inputArray.length - (r - l) - 1];

		System.arraycopy(inputArray, 0, newArray, 0, l);
		System.arraycopy(inputArray, r + 1, newArray, l, inputArray.length - r - 1);

		return newArray;
	}
}
