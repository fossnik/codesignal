import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		int[] inputArray = new int[]{2, 3, 2, 3, 4, 5};
		int[] expected = new int[]{2, 3, 5};
		assertArrayEquals(expected, example.removeArrayPart(inputArray, 2, 4));
	}

	@Test
	public void test2() {
		int[] inputArray = new int[]{2, 4, 10, 1};
		int[] expected = new int[]{1};
		assertArrayEquals(expected, example.removeArrayPart(inputArray, 0, 2));
	}

	@Test
	public void test3() {
		int[] inputArray = new int[]{5, 3, 2, 3, 4};
		int[] expected = new int[]{5, 2, 3, 4};
		assertArrayEquals(expected, example.removeArrayPart(inputArray, 1, 1));
	}

	@Test
	public void test4() {
		int[] inputArray = new int[]{1, 1};
		int[] expected = new int[]{};
		assertArrayEquals(expected, example.removeArrayPart(inputArray, 0, 1));
	}

	@Test
	public void test5() {
		int[] inputArray = new int[]{0, -2, 5, 6};
		int[] expected = new int[]{0, -2, 5};
		assertArrayEquals(expected, example.removeArrayPart(inputArray, 3, 3));
	}

	@Test
	public void test6() {
		int[] inputArray = new int[]{4, 3, 2, 1, 3, 4, 5};
		int[] expected = new int[]{4, 3, 4, 5};
		assertArrayEquals(expected, example.removeArrayPart(inputArray, 1, 3));
	}

	@Test
	public void test7() {
		int[] inputArray = new int[]{24, -40, -30, 30, 80, -94, 18, -56, -31, -68, -94, 67, -28, -2, -10, -83, -41, 43, -27, 0, -39, -83, -76, -59, -32, 87, -31, -55, -35, 20, 67, -78, -32, -90, 72, 15, -59, 4, 69, -82, 2, 96, -99, -78, 93, -68, -39, 68, 49, -9, -49, 77, 81, -55, -16, -9, -11, 80, 29, -6, 90, 83, 16, 68, -62, -73, -5, -86, 0, -48, 88, -35, 87, 12, 92, 12, 46, 57, 71, 91, -55, -62, -24, -78, -40, 30, -97, 64, -9, 40, 93, -67, -26, 53, -81, -7, -16, 14, -70, 88};
		int[] expected = new int[]{24, -40, -30, 30, 80, -94, 18, -56, -31, -68, -94, 67, -28, -2, -10, -83, -41, 43, -27, 88};
		assertArrayEquals(expected, example.removeArrayPart(inputArray, 19, 98));
	}

	@Test
	public void test8() {
		int[] inputArray = new int[]{66, -94, -83, -39, -27, 50, 58, 58, -68, -85, 55, -21, 83, -89, 52, -80, 17, -89, -56, 90, 75, 23, -74, -91, 93, -36, 90, 97, 52, 8, 0, -88, -5, -34, 55, 88, 96, -29, 30, -51, -69, 57, 85, -86, -47, 85, 77, 62, 55, 11, -4, 85, 32, 96, 69, 80, 78, -79, 70, 79, 77, 98, 85, 94, -34, 21, 5, 19, 85, 54, 50, 6, 31, -100, 74, -32, 35, 38, 59, 43, 89, 89, -60, 36, 46, 78, 43, 53, 84, -76, -24, -53, -5, 91, 100, 65, 23, 87, 20, 5};
		int[] expected = new int[]{};
		assertArrayEquals(expected, example.removeArrayPart(inputArray, 0, 99));
	}
}
