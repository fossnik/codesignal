import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(11, example.rangeBitCount(2, 7));
	}

	@Test
	public void test2() {
		assertEquals(1, example.rangeBitCount(0, 1));
	}

	@Test
	public void test3() {
		assertEquals(17, example.rangeBitCount(1, 10));
	}

	@Test
	public void test4() {
		assertEquals(3, example.rangeBitCount(8, 9));
	}

	@Test
	public void test5() {
		assertEquals(4, example.rangeBitCount(9, 10));
	}
}
