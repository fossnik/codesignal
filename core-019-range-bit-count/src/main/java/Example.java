class Example {
	int rangeBitCount(int a, int b) {
		int count = 0;
		for (int i = a; i <= b; i++) {
			count += Integer.toString(i, 2).replace("0", "").length();
		}

		return count;
	}
}
