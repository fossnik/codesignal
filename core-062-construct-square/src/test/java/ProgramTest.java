import org.junit.Test;

import static org.junit.Assert.*;

public class ProgramTest {

	private Program program = new Program();

	@Test
	public void test1() {
		assertEquals(81, program.constructSquare("ab"));
	}

	@Test
	public void test2() {
		assertEquals(-1, program.constructSquare("zzz"));
	}

	@Test
	public void test3() {
		assertEquals(900, program.constructSquare("aba"));
	}

	@Test
	public void test4() {
		assertEquals(810000, program.constructSquare("abcbbb"));
	}

	@Test
	public void test5() {
		assertEquals(961, program.constructSquare("abc"));
	}

	@Test
	public void test6() {
		assertEquals(999950884, program.constructSquare("aaaabbcde"));
	}
}
