import java.util.Arrays;
import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Program {
	int constructSquare(String s) {

		// get the rate of occurrence of characters
		Collection<Integer> inputCharTally = s.chars().boxed()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.summingInt(c -> 1)))
				.values();

		int largestPossibleNumber = (int) Math.pow(10, s.length()) - 1;

		// largest possible root
		int rootMax = (int) Math.sqrt(largestPossibleNumber);

		for (int i = rootMax; i > 1; i--) {
			String potentialSqrRoot = String.valueOf((int) Math.pow(i, 2));
			Collection<Integer> iCharTally = potentialSqrRoot
					.chars()
					.boxed()
					.collect(Collectors.groupingBy(Function.identity(), Collectors.summingInt(c -> 1)))
					.values();

			if (Arrays.equals(Arrays.stream(inputCharTally.toArray()).sorted().toArray(), iCharTally.stream().sorted().toArray())) {
				return (int) Math.pow(i, 2);
			}
		}

		return -1;
	}
}
