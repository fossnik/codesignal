import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(2, example.chessKnight("a1"));
	}

	@Test
	public void test2() {
		assertEquals(6, example.chessKnight("c2"));
	}

	@Test
	public void test3() {
		assertEquals(8, example.chessKnight("d4"));
	}

	@Test
	public void test4() {
		assertEquals(6, example.chessKnight("g6"));
	}

	@Test
	public void test5() {
		assertEquals(4, example.chessKnight("a3"));
	}

	@Test
	public void test6() {
		assertEquals(4, example.chessKnight("b7"));
	}

	@Test
	public void test7() {
		assertEquals(2, example.chessKnight("h8"));
	}

	@Test
	public void test8() {
		assertEquals(4, example.chessKnight("h6"));
	}

	@Test
	public void test9() {
		assertEquals(3, example.chessKnight("g8"));
	}

	@Test
	public void test10() {
		assertEquals(4, example.chessKnight("a5"));
	}
}
