import java.util.Arrays;

class Example {
	int chessKnight(String cell) {
		int x = cell.codePointAt(0) - 96;
		int y = Character.getNumericValue(cell.charAt(1));

		return (int) Arrays.stream(new int[][]{
				new int[] {1,2},
				new int[] {2,1},
				new int[] {2,-1},
				new int[] {1,-2},
				new int[] {-1,-2},
				new int[] {-2,-1},
				new int[] {-2,1},
				new int[] {-1,2},
		}).filter(ia -> x + ia[0] > 0 && x + ia[0] < 9 && y + ia[1] > 0 && y + ia[1] < 9).count();
	}
}
