import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(33, example.killKthBit(37, 3));
	}

	@Test
	public void test2() {
		assertEquals(37, example.killKthBit(37, 4));
	}

	@Test
	public void test3() {
		assertEquals(37, example.killKthBit(37, 2));
	}

	@Test
	public void test4() {
		assertEquals(0, example.killKthBit(0, 4));
	}

	@Test
	public void test5() {
		assertEquals(2147450879, example.killKthBit(2147483647, 16));
	}

	@Test
	public void test6() {
		assertEquals(374819652, example.killKthBit(374823748, 13));
	}

	@Test
	public void test7() {
		assertEquals(2734819, example.killKthBit(2734827, 4));
	}

	@Test
	public void test8() {
		assertEquals(1084197039, example.killKthBit(1084197039, 15));
	}

	@Test
	public void test9() {
		assertEquals(1160825067, example.killKthBit(1160825071, 3));
	}

	@Test
	public void test10() {
		assertEquals(2039063284, example.killKthBit(2039063284, 4));
	}

	@Test
	public void test11() {
		assertEquals(1981123330, example.killKthBit(1981123330, 7));
	}

	@Test
	public void test12() {
		assertEquals(427439517, example.killKthBit(427439517, 24));
	}

	@Test
	public void test13() {
		assertEquals(870676775, example.killKthBit(870677799, 11));
	}

	@Test
	public void test14() {
		assertEquals(1435387516, example.killKthBit(1435387516, 16));
	}

	@Test
	public void test15() {
		assertEquals(319033977, example.killKthBit(319033977, 3));
	}

	@Test
	public void test16() {
		assertEquals(773450695, example.killKthBit(773454791, 13));
	}

	@Test
	public void test17() {
		assertEquals(1986355154, example.killKthBit(2003132370, 25));
	}

	@Test
	public void test18() {
		assertEquals(2738, example.killKthBit(2738, 30));
	}

	@Test
	public void test19() {
		assertEquals(1073741823, example.killKthBit(2147483647, 31));
	}

	@Test
	public void test20() {
		assertEquals(5, example.killKthBit(5, 2));
	}
}
