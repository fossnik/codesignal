class Example {
	int killKthBit(int n, int k) {
		return n == 0 ? 0 : k > Integer.toString(n,2).length() ? n : Integer.parseInt(Integer.toString(n,2).chars().collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).replace(Integer.toString(n,2).length() - k, Integer.toString(n,2).length() - k + 1, "0").toString(), 2);
	}
}
