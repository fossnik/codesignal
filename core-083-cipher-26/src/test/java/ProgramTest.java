import org.junit.Test;

import static org.junit.Assert.*;

public class ProgramTest {

	private Program program = new Program();

	@Test
	public void test1() {
		String input = "taiaiaertkixquxjnfxxdh";
		assertEquals("thisisencryptedmessage", program.cipher26(input));
	}

	@Test
	public void test2() {
		String input = "ibttlprimfymqlpgeftwu";
		assertEquals("itsasecrettoeverybody", program.cipher26(input));
	}

	@Test
	public void test3() {
		String input = "ftnexvoky";
		assertEquals("fourtytwo", program.cipher26(input));
	}

	@Test
	public void test4() {
		String input = "taevzhzmashvjw";
		assertEquals("thereisnospoon", program.cipher26(input));
	}

	@Test
	public void test5() {
		String input = "abdgkpvcktdoanbqgxpicxtqon";
		assertEquals("abcdefghijklmnopqrstuvwxyz", program.cipher26(input));
	}

	@Test
	public void test6() {
		String input = "z";
		assertEquals("z", program.cipher26(input));
	}
}
