class Program {
	String cipher26(String message) {
		final int[] sum = {0};

		return message.codePoints().mapToObj(i -> {
			int previous = (i - 'a' - (sum[0] % 26) + 'a') % 26;
			sum[0] += previous;
			return (char) (previous + 'a');
		})
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
				.replace(0,1, message.substring(0,1))
				.toString();
	}
}
