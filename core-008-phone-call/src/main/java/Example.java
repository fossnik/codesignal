class Example {
	int phoneCall(int min1, int min2_10, int min11, int s) {
		return s > min1 + min2_10 * 9
				? (s - min1 - min2_10 * 9) / min11 + 10
				: s > min1 + min2_10
				? (s - min1) / min2_10 + 1
				: s >= min1
				? 1
				: 0;
	}
}
