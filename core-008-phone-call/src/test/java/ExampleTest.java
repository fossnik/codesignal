import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(14, example.phoneCall(3,1,2,20));
	}

	@Test
	public void test2() {
		assertEquals(1, example.phoneCall(2,2,1,2));
	}

	@Test
	public void test3() {
		assertEquals(11, example.phoneCall(10,1,2,22));
	}

	@Test
	public void test4() {
		assertEquals(14, example.phoneCall(2,2,1,24));
	}

	@Test
	public void test5() {
		assertEquals(3, example.phoneCall(1,2,1,6));
	}

	@Test
	public void test6() {
		assertEquals(0, example.phoneCall(10,10,10,8));
	}
}
