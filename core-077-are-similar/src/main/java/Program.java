public class Program {
	boolean areSimilar(int[] a, int[] b) {
		int indexOfFirstDissimilar = -1;

		for (int i = 0, count = 0; i < a.length; i++) {
			if (a[i] != b[i]) {
				count++;

				if (count > 2) return false;

				if (count == 1) {
					indexOfFirstDissimilar = i;
				}
				else if (count == 2) {
					// swap valid?
					if (a[indexOfFirstDissimilar] != b[i]) return false;
					if (b[indexOfFirstDissimilar] != a[i]) return false;
				}
			}
		}

		return true;
	}
}
