class Example {
	boolean isSmooth(int[] arr) {
		if (arr.length < 2) return false;

		int first = arr[0];
		int last = arr[arr.length - 1];
		int middle;

		if (arr.length % 2 != 0) {
			middle = arr[arr.length / 2];
		} else {
			int middleLeftIndex = arr.length / 2 - 1;
			int middleRightIndex = arr.length / 2;

			int middleLeftElement = arr[middleLeftIndex];
			int middleRightElement = arr[middleRightIndex];

			middle = middleLeftElement + middleRightElement;
		}
//		int middle = arr.length % 2 != 0
//				? arr[arr.length / 2]
//				: (arr[arr.length / 2] + arr[arr.length / 2 - 1]);

		return first == last && first == middle;
	}
}
