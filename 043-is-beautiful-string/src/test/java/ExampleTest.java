import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertTrue(Example.isBeautifulString("bbbaacdafe"));
	}

	@Test
	public void test2() {
		assertFalse(Example.isBeautifulString("aabbb"));
	}

	@Test
	public void test3() {
		assertFalse(Example.isBeautifulString("bbc"));
	}

	@Test
	public void test4() {
		assertFalse(Example.isBeautifulString("bbbaa"));
	}

	@Test
	public void test5() {
		assertFalse(Example.isBeautifulString("abcdefghijklmnopqrstuvwxyzz"));
	}

	@Test
	public void test6() {
		assertTrue(Example.isBeautifulString("abcdefghijklmnopqrstuvwxyz"));
	}

	@Test
	public void test7() {
		assertTrue(Example.isBeautifulString("abcdefghijklmnopqrstuvwxyzqwertuiopasdfghjklxcvbnm"));
	}

	@Test
	public void test8() {
		assertFalse(Example.isBeautifulString("fyudhrygiuhdfeis"));
	}

	@Test
	public void test9() {
		assertFalse(Example.isBeautifulString("zaa"));
	}

	@Test
	public void test10() {
		assertFalse(Example.isBeautifulString("zyy"));
	}
}
