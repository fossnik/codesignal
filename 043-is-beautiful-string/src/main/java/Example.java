import java.util.Map;
import java.util.TreeMap;

class Example {
	static boolean isBeautifulString(String inputString) {
		Map<Character, Integer> tally = new TreeMap<>();

		for (Character c : inputString.toCharArray()) {
			tally.put(c, tally.getOrDefault(c, 1) + 1);
		}

		if (Character.hashCode((Character) ((TreeMap) tally).firstKey()) + tally.size() - 1 != Character.hashCode((Character) ((TreeMap) tally).lastKey())) {
			return false;
		}

		int last = tally.getOrDefault('a', 0);
		for (Character c : tally.keySet()) {
			int count = tally.get(c);
			if (count > last) return false;
			last = count;
		}

		return true;
	}
}
