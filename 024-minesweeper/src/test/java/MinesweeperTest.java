import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class MinesweeperTest {

	@Test
	public void test1() {
		Minesweeper minesweeper = new Minesweeper();

		boolean[][] input = new boolean[][]{
				{true,false,false}, 
				{false,true,false}, 
				{false,false,false}
		};

		int[][] expected = new int[][]{
				{1,2,1},
				{2,1,1},
				{1,1,1}
		};

		assertArrayEquals(expected, minesweeper.minesweeper(input));
	}

	@Test
	public void test2() {
		Minesweeper minesweeper = new Minesweeper();

		boolean[][] input = new boolean[][]{
				{false,false,false},
				{false,false,false}
		};

		int[][] expected = new int[][]{
				{0,0,0},
				{0,0,0}
		};

		assertArrayEquals(expected, minesweeper.minesweeper(input));
	}

	@Test
	public void test3() {
		Minesweeper minesweeper = new Minesweeper();

		boolean[][] input = new boolean[][]{
				{true,false,false,true},
				{false,false,true,false},
				{true,true,false,true}
		};

		int[][] expected = new int[][]{
				{0,2,2,1},
				{3,4,3,3},
				{1,2,3,1}
		};

		assertArrayEquals(expected, minesweeper.minesweeper(input));
	}

	@Test
	public void test4() {
		Minesweeper minesweeper = new Minesweeper();

		boolean[][] input = new boolean[][]{
				{true,true,true},
				{true,true,true},
				{true,true,true}
		};

		int[][] expected = new int[][]{
				{3,5,3},
				{5,8,5},
				{3,5,3}
		};

		assertArrayEquals(expected, minesweeper.minesweeper(input));
	}

	@Test
	public void test5() {
		Minesweeper minesweeper = new Minesweeper();

		boolean[][] input = new boolean[][]{
				{false,true,true,false},
				{true,true,false,true},
				{false,false,true,false}
		};

		int[][] expected = new int[][]{
				{3,3,3,2},
				{2,4,5,2},
				{2,3,2,2}
		};


		assertArrayEquals(expected, minesweeper.minesweeper(input));
	}

	@Test
	public void test6() {
		Minesweeper minesweeper = new Minesweeper();

		boolean[][] input = new boolean[][]{
				{true,false},
				{true,false},
				{false,true},
				{false,false},
				{false,false}
		};

		int[][] expected = new int[][]{
				{1,2},
				{2,3},
				{2,1},
				{1,1},
				{0,0}
		};

		assertArrayEquals(expected, minesweeper.minesweeper(input));
	}
}
