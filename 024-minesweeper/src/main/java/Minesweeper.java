class Minesweeper {
	int[][] minesweeper(boolean[][] matrix) {

		int[][] remit = new int[matrix.length][matrix[0].length];

		for (int y = 0; y < matrix.length; y++) {
			for (int x = 0; x < matrix[0].length; x++) {
				remit[y][x] = countMines(x, y, matrix);
			}
		}

		return remit;
	}

	private int countMines(int x, int y, boolean[][] matrix) {
		int count = 0;

		int[][] directions = new int[][]{{-1,0}, {-1,1}, {0,1}, {1,1}, {1,0}, {1,-1}, {0,-1}, {-1,-1}};

		for (int[] yx : directions) { try {

			int Yaxis = y + yx[0];
			int Xaxis = x + yx[1];

			boolean isMine = matrix[Yaxis][Xaxis];
			if (isMine)
				count++;
			} catch (ArrayIndexOutOfBoundsException ignored) {}
		}

		return count;
	}
}