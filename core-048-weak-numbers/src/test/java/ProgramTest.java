import org.junit.Test;

import static org.junit.Assert.*;

public class ProgramTest {

	private Program program = new Program();

	@Test
	public void test1() {
		assertArrayEquals(new int[]{2, 2}, program.weakNumbers(9));
	}

	@Test
	public void test2() {
		assertArrayEquals(new int[]{0, 1}, program.weakNumbers(1));
	}

	@Test
	public void test3() {
		assertArrayEquals(new int[]{0, 2}, program.weakNumbers(2));
	}

	@Test
	public void test4() {
		assertArrayEquals(new int[]{2, 1}, program.weakNumbers(7));
	}

	@Test
	public void test5() {
		assertArrayEquals(new int[]{403, 1}, program.weakNumbers(500));
	}

	@Test
	public void test6() {
		assertArrayEquals(new int[]{0, 4}, program.weakNumbers(4));
	}
}
