import java.util.Arrays;

public class Program {
	int[] weakNumbers(int n) {
		int[] numDivisors = new int[n + 1];

		for (int i = 1; i <= n; i++) {
			int divisors = 0;
			for (int j = 2; j < i; j++) {
				if (i % j == 0) divisors++;
			}
			numDivisors[i] = divisors;
		}
		System.out.println(Arrays.toString(numDivisors));


		int[] weakness = new int[n + 1];

		for (int k = 1; k <= n; k++) {
			long count = 0L;
			long limit = k;
			for (int i : numDivisors) {
				if (limit-- == 0) break;
				if (numDivisors[k] < numDivisors[i]) {
					count++;
				}
			}
			weakness[k] = (int) count;
		}
		System.out.println(Arrays.toString(weakness));


		int weakest = Arrays.stream(weakness)
				.max()
				.getAsInt();

		int numOfWeakest = (int) Arrays.stream(weakness)
				.filter(i -> i == weakest)
				.count();

		System.out.printf("weakest: %s%nnumOfWekest: %s");
		return new int[]{weakest, numOfWeakest};
	}
}
