import org.junit.Test;

import static org.junit.Assert.*;

public class ProgramTest {

	private Program program = new Program();

	@Test
	public void test1() {
		int[][] input1 = new int[][]{
				{1,1,1},
 				{0,0}
		};
		int[][] input2 = new int[][]{
				{2,1,1},
 				{2,1}
		};
		assertTrue(program.areIsomorphic(input1, input2));
	}

	@Test
	public void test2() {
		int[][] input1 = new int[][]{
				{2},
 {}
		};
		int[][] input2 = new int[][]{
				{2}
		};
		assertFalse(program.areIsomorphic(input1, input2));
	}

	@Test
	public void test3() {
		int[][] input1 = new int[][]{
				{2},
 {1},
 {3,5}
		};
		int[][] input2 = new int[][]{
				{},
 {1},
 {1,6}
		};
		assertFalse(program.areIsomorphic(input1, input2));
	}

	@Test
	public void test4() {
		int[][] input1 = new int[][]{
				{1,1,1},
 {0,0}
		};
		int[][] input2 = new int[][]{
				{2,1,3},
 {2,0},
 {}
		};
		assertFalse(program.areIsomorphic(input1, input2));
	}

	@Test
	public void test5() {
		int[][] input1 = new int[][]{
				{},
 {1}
		};
		int[][] input2 = new int[][]{
				{},
 {6,2,5}
		};
		assertFalse(program.areIsomorphic(input1, input2));
	}

	@Test
	public void test6() {
		int[][] input1 = new int[][]{
				{1,3,4},
 {}
		};
		int[][] input2 = new int[][]{
				{2,1,2},
 {}
		};
		assertTrue(program.areIsomorphic(input1, input2));
	}

	@Test
	public void test7() {
		int[][] input1 = new int[][]{{}};
		int[][] input2 = new int[][]{{}};
		assertTrue(program.areIsomorphic(input1, input2));
	}

	@Test
	public void test8() {
		int[][] input1 = new int[][]{
				{2},
 {1},
 {3,50,33}
		};
		int[][] input2 = new int[][]{
				{},
 {1},
 {1,6,32}
		};
		assertFalse(program.areIsomorphic(input1, input2));
	}

	@Test
	public void test9() {
		int[][] input1 = new int[][]{
				{6},
 {3,5,2,4}
		};
		int[][] input2 = new int[][]{
				{34},
 {6,2,5}
		};
		assertFalse(program.areIsomorphic(input1, input2));
	}

	@Test
	public void test10() {
		int[][] input1 = new int[][]{
				{6},
 {3,5,2,4}
		};
		int[][] input2 = new int[][]{
				{34}, 
 {6,2,5,4}, 
 {1,2,3}
		};
		assertFalse(program.areIsomorphic(input1, input2));
	}
}
