import java.util.stream.IntStream;

public class Program {
	boolean areIsomorphic(int[][] array1, int[][] array2) {
		if (array1.length != array2.length) return false;

		return IntStream.range(0, array1.length)
				.allMatch(i -> array1[i].length == array2[i].length);
	}
}
