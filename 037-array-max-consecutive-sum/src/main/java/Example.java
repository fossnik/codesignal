import java.util.ArrayDeque;

class Example {
	static int arrayMaxConsecutiveSum(int[] inputArray, int k) {
		int max = 0;

		ArrayDeque<Integer> queue = new ArrayDeque<>();

		int runningSum = 0;
		for (int i : inputArray) {

			if (queue.size() == k) {
				runningSum -= queue.pop();
			}

			runningSum += i;
			queue.add(i);

			if (runningSum > max) max = runningSum;
		}

		return max;
	}
}
