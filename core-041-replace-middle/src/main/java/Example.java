class Example {
	int[] replaceMiddle(int[] arr) {
		if (arr.length % 2 != 0) return arr;

		int middleLeftIndex = arr.length / 2 - 1;
		int middleRightIndex = arr.length / 2;
		int middleLeftElement = arr[middleLeftIndex];
		int middleRightElement = arr[middleRightIndex];

		int newMiddleElement = middleLeftElement + middleRightElement;

		int[] newArray = new int[arr.length - 1];

		System.arraycopy(arr, 0, newArray, 0, middleLeftIndex);

		newArray[middleLeftIndex] = newMiddleElement;

		System.arraycopy(arr, middleRightIndex + 1, newArray, middleRightIndex, middleLeftIndex);

		return newArray;
	}
}
