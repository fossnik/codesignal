import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertArrayEquals(new int[]{7, 2, 7, 10, 7}, example.replaceMiddle(new int[]{7, 2, 2, 5, 10, 7}));
	}

	@Test
	public void test2() {
		assertArrayEquals(new int[]{-5, -5, 10}, example.replaceMiddle(new int[]{-5, -5, 10}));
	}

	@Test
	public void test3() {
		assertArrayEquals(new int[]{45, 23, 12, 45, 453, -234, -45}, example.replaceMiddle(new int[]{45, 23, 12, 33, 12, 453, -234, -45}));
	}

	@Test
	public void test4() {
		assertArrayEquals(new int[]{10}, example.replaceMiddle(new int[]{2, 8}));
	}

	@Test
	public void test5() {
		assertArrayEquals(new int[]{-12, 34, 40, -5, -12, 4, 0, 0, -12}, example.replaceMiddle(new int[]{-12, 34, 40, -5, -12, 4, 0, 0, -12}));
	}

	@Test
	public void test6() {
		assertArrayEquals(new int[]{9, 15, 9}, example.replaceMiddle(new int[]{9, 0, 15, 9}));
	}

	@Test
	public void test7() {
		assertArrayEquals(new int[]{-6, 6, -6}, example.replaceMiddle(new int[]{-6, 6, -6}));
	}

	@Test
	public void test8() {
		assertArrayEquals(new int[]{26, 26, -17}, example.replaceMiddle(new int[]{26, 26, -17}));
	}

	@Test
	public void test9() {
		assertArrayEquals(new int[]{-7, 10, 10}, example.replaceMiddle(new int[]{-7, 5, 5, 10}));
	}
}
