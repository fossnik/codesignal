class Example {
	int secondRightmostZeroBit(int n) {
		return (int) Math.pow(2, Integer.toString(n,2).length() - Integer.toString(n,2).substring(0,Integer.toString(n,2).lastIndexOf('0')).lastIndexOf('0') - 1);
	}
}
