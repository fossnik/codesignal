import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(8, example.secondRightmostZeroBit(37));
	}

	@Test
	public void test2() {
		assertEquals(2, example.secondRightmostZeroBit(1073741824));
	}

	@Test
	public void test3() {
		assertEquals(2, example.secondRightmostZeroBit(83748));
	}

	@Test
	public void test4() {
		assertEquals(2, example.secondRightmostZeroBit(4));
	}

	@Test
	public void test5() {
		assertEquals(4, example.secondRightmostZeroBit(728782938));
	}
}
