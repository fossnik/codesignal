class DepositProfit {
	int depositProfit(int deposit, int rate, int threshold) {
		int years = 0;

		float depositF = (float) deposit,
				rateF = (float) rate,
				thresholdF = (float) threshold;

		while (depositF < thresholdF) {
			years++;
			depositF += depositF * (rateF / 100F);
		}

		return years;
	}

}
