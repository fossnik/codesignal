import org.junit.Test;

import static org.junit.Assert.*;

public class DepositProfitTest {

	private DepositProfit depositProfit = new DepositProfit();

	@Test
	public void test1() {
		assertEquals(3, depositProfit.depositProfit(100, 20, 170));
	}

	@Test
	public void test2() {
		assertEquals(1, depositProfit.depositProfit(100, 1, 101));
	}

	@Test
	public void test3() {
		assertEquals(6, depositProfit.depositProfit(1, 100, 64));
	}

	@Test
	public void test4() {
		assertEquals(6, depositProfit.depositProfit(20, 20, 50));
	}

	@Test
	public void test5() {
		assertEquals(4, depositProfit.depositProfit(50, 25, 100));
	}

	@Test
	public void test6() {
		assertEquals(1, depositProfit.depositProfit(70, 10, 77));
	}

	@Test
	public void test7() {
		assertEquals(533, depositProfit.depositProfit(1, 1, 200));
	}

	@Test
	public void test8() {
		assertEquals(463, depositProfit.depositProfit(1, 1, 100));
	}

	@Test
	public void test9() {
		assertEquals(6, depositProfit.depositProfit(60, 5, 80));
	}

	@Test
	public void test10() {
		assertEquals(7, depositProfit.depositProfit(1, 100, 128));
	}
}
