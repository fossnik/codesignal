class Example {
	int rounders(int n) {
		StringBuilder number = new StringBuilder(Integer.toString(n));

		boolean carryOver = false;
		for (int i = number.length() - 1; i >= 1; i--) {
			if (carryOver) {
				if (Integer.parseInt(number.charAt(i) + "") + 1 >= 5) {
					carryOver = true;
				} else {
					carryOver = false;
				}
			} else {
				if (Integer.parseInt(number.charAt(i) + "") + 0 >= 5) {
					carryOver = true;
				} else {
					carryOver = false;
				}
			}

			number.setCharAt(i, '0');
		}

		if (carryOver) {
			if (number.charAt(0) == '9') {
				number.setCharAt(0, '1');
				number.append('0');
			} else {
				char plusOne = Integer.toString(Integer.parseInt(number.charAt(0) + "") + 1).charAt(0);
				number.setCharAt(0, plusOne);
			}
		}

		return Integer.parseInt(number.toString());
	}

}
