import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(20, example.rounders(15));
	}

	@Test
	public void test2() {
		assertEquals(1000, example.rounders(1234));
	}

	@Test
	public void test3() {
		assertEquals(2000, example.rounders(1445));
	}

	@Test
	public void test4() {
		assertEquals(10, example.rounders(14));
	}

	@Test
	public void test5() {
		assertEquals(10, example.rounders(10));
	}

	@Test
	public void test6() {
		assertEquals(7000, example.rounders(7001));
	}

	@Test
	public void test7() {
		assertEquals(100, example.rounders(99));
	}
}
