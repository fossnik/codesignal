class Example {
	int leastFactorial(int n) {
		for (int m = 1, sum = 1;; sum *= m++) {
			if (sum >= n) return sum;
		}
	}
}
