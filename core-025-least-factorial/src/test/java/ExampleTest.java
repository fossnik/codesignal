import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(24, example.leastFactorial(17));
	}

	@Test
	public void test2() {
		assertEquals(1, example.leastFactorial(1));
	}

	@Test
	public void test3() {
		assertEquals(6, example.leastFactorial(5));
	}

	@Test
	public void test4() {
		assertEquals(120, example.leastFactorial(25));
	}

	@Test
	public void test5() {
		assertEquals(24, example.leastFactorial(18));
	}

	@Test
	public void test6() {
		assertEquals(120, example.leastFactorial(109));
	}

	@Test
	public void test7() {
		assertEquals(120, example.leastFactorial(106));
	}

	@Test
	public void test8() {
		assertEquals(24, example.leastFactorial(11));
	}

	@Test
	public void test9() {
		assertEquals(120, example.leastFactorial(55));
	}

	@Test
	public void test10() {
		assertEquals(24, example.leastFactorial(24));
	}
}
