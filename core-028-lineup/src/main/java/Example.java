public class Example {
	int lineUp(String commands) {
		int count = 0;
		dir student1 = dir.NORTH;
		dir student2 = dir.NORTH;

		for (char c : commands.toCharArray()) {
			switch(c) {
				case 'L':
					student1 = dir.values()[((student1.ordinal() + 1 + 4) % 4)];
					student2 = dir.values()[((student2.ordinal() - 1 + 4) % 4)];
					break;

				case 'R':
					student1 = dir.values()[((student1.ordinal() - 1 + 4) % 4)];
					student2 = dir.values()[((student2.ordinal() + 1 + 4) % 4)];
					break;

				case 'A':
					student1 = dir.values()[((student1.ordinal() + 2 + 4) % 4)];
					student2 = dir.values()[((student2.ordinal() + 2 + 4) % 4)];
					break;
			}
			if (student1.equals(student2)) count++;
		}

		return count;
	}

	enum dir {
		NORTH, EAST, SOUTH, WEST
	}
}
