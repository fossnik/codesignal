import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(3, example.lineUp("LLARL"));
	}

	@Test
	public void test2() {
		assertEquals(1, example.lineUp("RLR"));
	}

	@Test
	public void test3() {
		assertEquals(0, example.lineUp(""));
	}

	@Test
	public void test4() {
		assertEquals(0, example.lineUp("L"));
	}

	@Test
	public void test5() {
		assertEquals(1, example.lineUp("A"));
	}

	@Test
	public void test6() {
		assertEquals(15, example.lineUp("AAAAAAAAAAAAAAA"));
	}

	@Test
	public void test7() {
		assertEquals(16, example.lineUp("RRRRRRRRRRLLLLLLLLLRRRRLLLLLLLLLL"));
	}

	@Test
	public void test8() {
		assertEquals(5, example.lineUp("AALAAALARAR"));
	}
}
