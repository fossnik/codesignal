import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(52, example.deleteDigit(152));
	}

	@Test
	public void test2() {
		assertEquals(101, example.deleteDigit(1001));
	}

	@Test
	public void test3() {
		assertEquals(1, example.deleteDigit(10));
	}

	@Test
	public void test4() {
		assertEquals(22229, example.deleteDigit(222219));
	}

	@Test
	public void test5() {
		assertEquals(19, example.deleteDigit(109));
	}

	@Test
	public void test6() {
		assertEquals(22250, example.deleteDigit(222250));
	}

	@Test
	public void test7() {
		assertEquals(4445, example.deleteDigit(44435));
	}

	@Test
	public void test8() {
		assertEquals(2, example.deleteDigit(12));
	}

	@Test
	public void test9() {
		assertEquals(28616, example.deleteDigit(218616));
	}

	@Test
	public void test10() {
		assertEquals(86452, example.deleteDigit(861452));
	}
}
