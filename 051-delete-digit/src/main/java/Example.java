class Example {
	int deleteDigit(int n) {
		int length = String.valueOf(n).length();
		int max = 0;
		for (int i = 0; i < length; i++) {
			StringBuilder test = new StringBuilder(String.valueOf(n));
			test.deleteCharAt(i);
			if (Integer.valueOf(test.toString()) > max) {
				max = Integer.valueOf(test.toString());
			}
		}

		return max;
	}
}
