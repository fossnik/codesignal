import java.util.Arrays;

class AbsoluteValuesSumMinimization {
	int absoluteValuesSumMinimization(int[] a) {
		int min = Arrays.stream(a).map(i -> Math.abs(i - a[0])).sum();
		int xbest = a[0];

		for (int x : a) {
			int sum = Arrays.stream(a).map(i -> Math.abs(i - x)).sum();

			if (sum < min) {
				min = sum;
				xbest = x;
			}
		}

		return xbest;
	}
}
