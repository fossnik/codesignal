import java.util.Arrays;

class Example {
	String messageFromBinaryCode(String code) {
		return Arrays.stream(splitInput(code, 8)).mapToInt(s -> {
			int i = 128;
			int value = 0;
			for (char c : s.toCharArray()) {
				value += Character.getNumericValue(c) * i;
				i /= 2;
			}
			return value;
		})
		.collect(StringBuilder::new,
				(sb, i) -> sb.append((char) i),
				StringBuilder::append)
		.toString();
	}

	private String[] splitInput(String inputString, int segmentSize) {
		String re = "(?<=\\G.{" + segmentSize + "})";
		return inputString.split(re);
	}
}
