class ChessBoardCellColor {
	boolean chessBoardCellColor(String cell1, String cell2) {
		return
			(((int) cell1.charAt(0)) % 2 == 0 ^ ((int) cell1.charAt(1)) % 2 == 0)
						==
			(((int) cell2.charAt(0)) % 2 == 0 ^ ((int) cell2.charAt(1)) % 2 == 0);
	}
}
