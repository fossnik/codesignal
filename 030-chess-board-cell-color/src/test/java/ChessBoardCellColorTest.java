import org.junit.Test;

import static org.junit.Assert.*;

public class ChessBoardCellColorTest {

	private ChessBoardCellColor alphabeticShift = new ChessBoardCellColor();

	@Test
	public void test1() {
		assertTrue(alphabeticShift.chessBoardCellColor("A1", "C3"));
	}

	@Test
	public void test2() {
		assertFalse(alphabeticShift.chessBoardCellColor("A1", "H3"));
	}

	@Test
	public void test3() {
		assertFalse(alphabeticShift.chessBoardCellColor("A1", "A2"));
	}

	@Test
	public void test4() {
		assertTrue(alphabeticShift.chessBoardCellColor("A1", "B2"));
	}

	@Test
	public void test5() {
		assertFalse(alphabeticShift.chessBoardCellColor("B3", "H8"));
	}

	@Test
	public void test6() {
		assertFalse(alphabeticShift.chessBoardCellColor("C3", "B5"));
	}

	@Test
	public void test7() {
		assertTrue(alphabeticShift.chessBoardCellColor("G5", "E7"));
	}

	@Test
	public void test8() {
		assertFalse(alphabeticShift.chessBoardCellColor("C8", "H8"));
	}

	@Test
	public void test9() {
		assertTrue(alphabeticShift.chessBoardCellColor("D2", "D2"));
	}

	@Test
	public void test10() {
		assertFalse(alphabeticShift.chessBoardCellColor("A2", "A5"));
	}
}
