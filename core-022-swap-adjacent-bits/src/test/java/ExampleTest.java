import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(14, example.swapAdjacentBits(13));
	}

	@Test
	public void test2() {
		assertEquals(133, example.swapAdjacentBits(74));
	}

	@Test
	public void test3() {
		assertEquals(1073741823, example.swapAdjacentBits(1073741823));
	}

	@Test
	public void test4() {
		assertEquals(0, example.swapAdjacentBits(0));
	}

	@Test
	public void test5() {
		assertEquals(2, example.swapAdjacentBits(1));
	}

	@Test
	public void test6() {
		assertEquals(166680, example.swapAdjacentBits(83748));
	}
}
