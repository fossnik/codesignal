class AlphabeticShift {
	String alphabeticShift(String inputString) {
		return inputString.chars().mapToObj(i -> Character.isLowerCase((char) i) ? (char)((i - (int)'a' + 1) % 26 + (int)'a') : (char)((i - (int)'A' + 1) % 26 + (int)'A')).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
	}
}
