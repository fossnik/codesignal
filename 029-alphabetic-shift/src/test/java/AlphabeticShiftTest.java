import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AlphabeticShiftTest {

	@Test
	public void test1() {
		AlphabeticShift alphabeticShift = new AlphabeticShift();

		assertEquals("dsbaz", alphabeticShift.alphabeticShift("crazy"));
	}

	@Test
	public void test2() {
		AlphabeticShift alphabeticShift = new AlphabeticShift();

		assertEquals("a", alphabeticShift.alphabeticShift("z"));
	}

	@Test
	public void test3() {
		AlphabeticShift alphabeticShift = new AlphabeticShift();

		assertEquals("bbbbcccdde", alphabeticShift.alphabeticShift("aaaabbbccd"));
	}

	@Test
	public void test4() {
		AlphabeticShift alphabeticShift = new AlphabeticShift();

		assertEquals("gvaaz", alphabeticShift.alphabeticShift("fuzzy"));
	}

	@Test
	public void test5() {
		AlphabeticShift alphabeticShift = new AlphabeticShift();

		assertEquals("dpeftjhobm", alphabeticShift.alphabeticShift("codesignal"));
	}
}
