import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		String[] input = {"aba", "bbb", "bab"};

		assertFalse(example.stringsRearrangement(input));
	}

	@Test
	public void test2() {
		String[] input = {"ab", "bb", "aa"};

		assertTrue(example.stringsRearrangement(input));
	}

	@Test
	public void test3() {
		String[] input = {"q", "q"};

		assertFalse(example.stringsRearrangement(input));
	}

	@Test
	public void test4() {
		String[] input = {"zzzzab", "zzzzbb", "zzzzaa"};

		assertTrue(example.stringsRearrangement(input));
	}

	@Test
	public void test5() {
		String[] input = {"ab", "ad", "ef", "eg"};

		assertFalse(example.stringsRearrangement(input));
	}

	@Test
	public void test6() {
		String[] input = {"abc", "bef", "bcc", "bec", "bbc", "bdc"};

		assertTrue(example.stringsRearrangement(input));
	}

	@Test
	public void test7() {
		String[] input = {"abc", "abx", "axx", "abc"};

		assertFalse(example.stringsRearrangement(input));
	}

	@Test
	public void test8() {
		String[] input = {"abc", "abx", "axx", "abx", "abc"};

		assertTrue(example.stringsRearrangement(input));
	}

	@Test
	public void test9() {
		String[] input = {"f", "g", "a", "h"};

		assertTrue(example.stringsRearrangement(input));
	}

	@Test
	public void test10() {
		String[] input = {"ff", "gf", "af", "ar", "hf"};

		assertTrue(example.stringsRearrangement(input));
	}

	@Test
	public void test11() {
		String[] input = {"a", "b", "c"};

		assertTrue(example.stringsRearrangement(input));
	}
}
