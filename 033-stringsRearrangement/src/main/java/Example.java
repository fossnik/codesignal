import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Example {
	boolean stringsRearrangement(String[] inputArray) {
		String[] sorted = Arrays.stream(inputArray).sorted().toArray(String[]::new);

		// test input that does not include duplicates
		if (inputArray.length == Arrays.stream(inputArray).distinct().count()
				&& allStringsConform(sorted)) return true;

		// test starting with each individual input string
		for (String string : Arrays.stream(sorted).distinct().toArray(String[]::new)) {
			ArrayDeque<String> input = new ArrayDeque<>(Arrays.stream(sorted).collect(Collectors.toList()));
			input.remove(string);
			ArrayDeque<String> output = new ArrayDeque<>(Collections.singletonList(string));
			if (testPermutations(input, output)) return true;
		}

		return false;
	}

	private boolean testPermutations(ArrayDeque<String> input, ArrayDeque<String> output) {
		// base case
		if (input.isEmpty()) return true;

		List<String> validNextStrings = getMatches(output.getLast(), input);
		if (validNextStrings.isEmpty()) return false;

		for (String possible : validNextStrings) {
			ArrayDeque<String> newInput = input.clone();
			newInput.remove(possible);
			ArrayDeque<String> newOutput = output.clone();
			newOutput.add(possible);

			if (testPermutations(newInput, newOutput))
				return true;
		}

		return false;
	}

	private List<String> getMatches(String last, ArrayDeque<String> input) {
		return input.stream()
				.filter(prospect -> twoStringsVaryExactlyOnce(last, prospect))
				.collect(Collectors.toList());
	}

	private boolean allStringsConform(String[] strings) {
		return IntStream.range(0, strings.length - 1).allMatch(i -> twoStringsVaryExactlyOnce(strings[i], strings[i + 1]));
	}

	private static boolean twoStringsVaryExactlyOnce(String a, String b) {
		return 1 == IntStream.range(0, a.length()).filter(i -> a.charAt(i) != b.charAt(i)).count();
	}
}
