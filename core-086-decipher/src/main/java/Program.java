public class Program {
	String decipher(String cipher) {
		StringBuilder remit = new StringBuilder();

		for (int i = 0; i < cipher.length(); i+=2) {
			int c = Integer.valueOf(cipher.substring(i, i + 2));
			if (Character.isLowerCase(c)) remit.append((char) c);
			else {
				c = Integer.valueOf(cipher.substring(i, ++i + 2));
				if (Character.isLowerCase(c)) remit.append((char) c);
			}
		}

		return remit.toString();
	}

}
