import org.junit.Test;

import static org.junit.Assert.*;

public class ProgramTest {

	private Program program = new Program();

	@Test
	public void test1() {
		String input = "10197115121";
		assertEquals("easy", program.decipher(input));
	}

	@Test
	public void test2() {
		String input = "98";
		assertEquals("b", program.decipher(input));
	}

	@Test
	public void test3() {
		String input = "122";
		assertEquals("z", program.decipher(input));
	}

	@Test
	public void test4() {
		String input = "1229897";
		assertEquals("zba", program.decipher(input));
	}

	@Test
	public void test5() {
		String input = "97989910010110210310410510610710810911011111211311411511611711811912012112297";
		assertEquals("abcdefghijklmnopqrstuvwxyza", program.decipher(input));
	}

	@Test
	public void test6() {
		String input = "10297115106108102108971061151041029897107106115981001029710711510010298";
		assertEquals("fasjlflajshfbakjsbdfaksdfb", program.decipher(input));
	}

	@Test
	public void test7() {
		String input = "11211111911310110810910097107108115111112119113101106107971101021101061021041149710511411497";
		assertEquals("powqelmdaklsopwqejkanfnjfhrairra", program.decipher(input));
	}
}
