class Example {
	int additionWithoutCarrying(int param1, int param2) {
		String[] smaller = Integer.toString(Math.min(param1, param2)).split("");
		String[] larger = Integer.toString(Math.max(param1, param2)).split("");

		smaller = String.format("%" + larger.length + "s", String.join("", smaller)).replace(' ', '0').split("");

		StringBuilder output = new StringBuilder();
		for (int i = 0; i < smaller.length; i++) {
			output.append((Integer.valueOf(smaller[i]) + Integer.valueOf(larger[i])) % 10);
		}

		return Integer.parseInt(output.toString());
	}
}
