import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(1180, example.additionWithoutCarrying(456, 1734));
	}

	@Test
	public void test2() {
		assertEquals(99999, example.additionWithoutCarrying(99999, 0));
	}

	@Test
	public void test3() {
		assertEquals(888, example.additionWithoutCarrying(999, 999));
	}

	@Test
	public void test4() {
		assertEquals(0, example.additionWithoutCarrying(0, 0));
	}

	@Test
	public void test5() {
		assertEquals(8642, example.additionWithoutCarrying(54321, 54321));
	}

	@Test
	public void test6() {
		assertEquals(0, example.additionWithoutCarrying(54321, 56789));
	}

//	@Test
//	public void test7() {
//		assertEquals(, example.additionWithoutCarrying(,));
//	}
//
//	@Test
//	public void test8() {
//		assertEquals(, example.additionWithoutCarrying(,));
//	}
//
//	@Test
//	public void test9() {
//		assertEquals(, example.additionWithoutCarrying(,));
//	}
//
//	@Test
//	public void test10() {
//		assertEquals(, example.additionWithoutCarrying(,));
//	}
}
