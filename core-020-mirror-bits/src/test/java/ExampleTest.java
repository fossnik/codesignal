import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(67, example.mirrorBits(97));
	}

	@Test
	public void test2() {
		assertEquals(1, example.mirrorBits(8));
	}

	@Test
	public void test3() {
		assertEquals(111, example.mirrorBits(123));
	}

	@Test
	public void test4() {
		assertEquals(65173, example.mirrorBits(86782));
	}

	@Test
	public void test5() {
		assertEquals(5, example.mirrorBits(5));
	}
}
