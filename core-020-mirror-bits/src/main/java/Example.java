import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

class Example {
	int mirrorBits(int a) {
		List<String> numbers = Arrays.stream(Integer.toString(a, 2).split("")).collect(Collectors.toList());

		Collections.reverse(numbers);

		return Integer.parseInt(Arrays.stream(numbers.toArray(String[]::new)).collect(Collectors.joining("")), 2);
	}
}
