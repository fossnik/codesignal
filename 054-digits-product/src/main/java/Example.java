import java.util.*;
import java.util.stream.Collectors;

class Example {
	int digitsProduct(int product) {
		TreeSet<Integer> validDs = new TreeSet<>();

		int[] factors = getFactors(product)
				.stream()
				.mapToInt(Integer::intValue)
				.filter(integer -> integer < 10)
				.toArray();

		// just try to brute-force it
		HashSet<LinkedList<Integer>> testedPerms = new HashSet<>();
		Random rand = new Random();
		LinkedList<Integer> perm;
		LinkedList<Integer> pool;

		// number of possible permutations for each
		int[] numPossblPerms = new int[factors.length];
		for (int i = 1; i < factors.length; i++) {
			numPossblPerms[i] = factorial(i) + numPossblPerms[i - 1];
		}

		int numDigits = 2;
		while (testedPerms.size() < numPossblPerms[factors.length - 1]) {
			do {
				perm = new LinkedList<>();
				pool = Arrays.stream(factors).boxed().collect(Collectors.toCollection(LinkedList::new));

				for (int i = 0; i < numDigits; i++) {
					perm.add(pool.remove(rand.nextInt(pool.size())));
				}

			} while (testedPerms.contains(perm));
			testedPerms.add(perm);

			int p = Arrays
					.stream(perm.toArray())
					.mapToInt(o -> (int) o)
					.reduce(1, (a, b) -> a * b);

			if (p == product) validDs.add(Integer.valueOf(perm.toString().replaceAll("\\D", "")));

			if (testedPerms.size() == numPossblPerms[numDigits]) {
				if (!validDs.isEmpty()) return Collections.min(validDs);
				else if (numPossblPerms.length < numDigits) numDigits++;
			}
		}

		return -1;
	}

	private ArrayList<Integer> getFactors(int n) {
		ArrayList<Integer> factors = new ArrayList<>();
		for (int i = 1; i <= Math.sqrt(n); i += 1) {
			if (n % i == 0) {
				factors.add(i);
				if (i != n / i) factors.add(n / i);
			}
		}

		return factors
				.stream()
				.mapToInt(Integer::intValue)
				.boxed()
				.collect(Collectors.toCollection(ArrayList::new));
	}

	private int factorial(int number) {
		int result = 1;

		for (int factor = 2; factor <= number; factor++) {
			result *= factor;
		}

		return result;
	}

//	private int howMuch(int number) {
//		HashSet<String> stringHashSet = new HashSet<>();
//		ArrayList<Character> keyspace = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".chars().limit(number).mapToObj(e -> (char) e).collect(Collectors.toCollection(ArrayList::new));
//		Random random = new Random();
//
//		for (int i = 0; i < 1000000; i++) {
//			LinkedList<Character> pool = new LinkedList<>(keyspace);
//
//			StringBuilder sb = new StringBuilder();
//			while (sb.length() < number) {
//				sb.append(pool.remove(random.nextInt(pool.size())));
//			}
//
//			stringHashSet.add(sb.toString());
//		}
//
//		return stringHashSet.size();
//	}
}
