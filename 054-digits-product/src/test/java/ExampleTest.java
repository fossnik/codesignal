import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleTest {

	private Example example = new Example();

	@Test
	public void test1() {
		assertEquals(26, example.digitsProduct(12));
	}

	@Test
	public void test2() {
		assertEquals(-1, example.digitsProduct(19));
	}

	@Test
	public void test3() {
		assertEquals(2559, example.digitsProduct(450));
	}

	@Test
	public void test4() {
		assertEquals(1, example.digitsProduct(0));
	}

	@Test
	public void test5() {
		assertEquals(-1, example.digitsProduct(13));
	}

	@Test
	public void test6() {
		assertEquals(1, example.digitsProduct(1));
	}

	@Test
	public void test7() {
		assertEquals(399, example.digitsProduct(243));
	}

	@Test
	public void test8() {
		assertEquals(889, example.digitsProduct(576));
	}

	@Test
	public void test9() {
		assertEquals(589, example.digitsProduct(360));
	}
}
